Find the values for (SEND,MORE,MONEY) such that:

    S E N D
  + M O R E
  =========
  M O N E Y
  =========

Each letter represents its own digit (0-9) and multiple occurrences of the same letter represent
the same digit (eg if one of the E's represents a 3, they all do).

\begin{code}

import Control.Monad             (guard)
import Control.Monad.Trans.State (StateT(..), evalStateT)
import Data.List                 (foldl1)

-- | Select all unique characters from the list, and retain the remainder of the list
--   after selecting each character.
--   Example: select "abc" = [('a',"bc"), ('b',"ac"), ('c',"ab")]
select :: [a] -> [(a, [a])]
select [] = []
select (x:xs) = (x,xs) : [ (y,x:ys) | (y,ys) <- select xs ]

-- | Calculate the decimal representation of the number from a list of numbers.
--   Example: calculate [1,2,3] = 123
calculate :: [Int] -> Int
calculate = foldl1 (\a b -> a * 10 + b)

-- | Create a state transformer which has an initial state [a] and returns a
--   new state [a] with one element less, after returning that element as the result.
stateTransformer :: StateT [a] [] a
stateTransformer = StateT select

-- | Although we can cut down on the search path by intuitively reasoning,
--   we brute force it using only the information we know from the problem.
--   No two letters signify the same digit, so, we use a state transformer
--   which takes the initial state (numbers 0-9) and returns a new state
--   with all the numbers except the one selected.
--   s and m cannot be zero since they are the first digit.
computation :: StateT [Int] [] (Int, Int, Int)
computation = do
  s <- stateTransformer
  e <- stateTransformer
  n <- stateTransformer
  d <- stateTransformer
  m <- stateTransformer
  o <- stateTransformer
  r <- stateTransformer
  y <- stateTransformer
  guard $ s /= 0 && m /= 0
  let send  = calculate [s,e,n,d]
      more  = calculate [m,o,r,e]
      money = calculate [m,o,n,e,y]
  guard $ send + more == money
  return (send, more, money)

main :: IO ()
main = print $ evalStateT computation [0..9]

\end{code}

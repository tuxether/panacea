Digit factorials
================

Problem 34
----------

145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.

Solution
--------

\begin{code}

import Data.Char (digitToInt)

factorial n = product [1..n]

digitFactorial = sum . map (factorial . digitToInt) . show

curiousNumbers = [ n | n <- [10..100000], n == digitFactorial n ]

main = print . sum $ curiousNumbers

\end{code}


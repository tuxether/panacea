Consecutive prime sum
=====================

Problem 50
-----------

The prime 41, can be written as the sum of six consecutive primes:

41 = 2 + 3 + 5 + 7 + 11 + 13

This is the longest sum of consecutive primes that adds to a prime below one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.

Which prime, below one-million, can be written as the sum of the most consecutive primes?

Solution
---------

\begin{code}

import Data.Function (fix)
import Data.List (inits)

limit :: Integral a => a
limit = 10^6

isPrime :: Integral a => a -> Bool
isPrime n = isPrime_ n primes
  where
    isPrime_ :: Integral a => a -> [a] -> Bool
    isPrime_ n (p:ps)
      | p * p > n      = True
      | n `mod` p == 0 = False
      | otherwise      = isPrime_ n ps

primes :: Integral a => [a]
primes = 2 : filter isPrime [3,5..]

recursiveTail :: Int -> [a] -> [[a]]
recursiveTail = fix (\f n xs -> take n xs : f n (tail xs))

maxConsecutives :: Int
maxConsecutives = length . takeWhile (< limit) . scanl1 (+) $ primes

consecutive :: Integral a => Int -> [[a]]
consecutive n = filter (\xs -> isPrime (sum xs))
              . takeWhile (\xs -> sum xs < limit)
              . recursiveTail n
              $ primes

problem_50 = head
           . fmap sum
           . head
           . filter (not . null)
           . reverse
           . fmap consecutive
           $ [2..maxConsecutives]

main = print $ problem_50

\end{code}


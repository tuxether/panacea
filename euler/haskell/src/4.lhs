Largest palindrome product
==========================

Problem 4
---------

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

Solution
--------

\begin{code}

range = [999,998..100]

isPalindrome n = (reverse . show) n == show n

main = print $ maximum [x | y <- range, z <- range, let x = y * z, isPalindrome x]

\end{code}

Double-base palindromes
=======================

Problem 36
----------

The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million,
which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)

\begin{code}

isPalindrome :: Integer -> Bool
isPalindrome n = show n == (reverse . show) n

isDoublePalindrome :: Integer -> Bool
isDoublePalindrome n = isPalindrome n && isPalindrome (decimalToBinary n)

decimalToBinary :: Integer -> Integer
decimalToBinary = listToInt . toBinary

listToInt :: [Integer] -> Integer
listToInt = foldl1 (\a b -> a * 10 + b)

toBinary :: Integer -> [Integer]
toBinary 0 = [0]
toBinary n = toBinary (n `quot` 2) ++ [n `rem` 2]

main = print $ sum [x | x <- [1..1000000], isDoublePalindrome x]

\end{code}

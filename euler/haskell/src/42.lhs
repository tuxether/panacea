Coded triangle numbers
======================

Problem 42
-----------

The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle numbers are:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.

Using 42.txt, a 16K text file containing nearly two-thousand common English words, how many are triangle words?

Solution
--------

\begin{code}

import Data.Char (ord)
import Data.Monoid ( (<>) )

wordValue :: String -> Int
wordValue = sum . fmap alphOrd
  where alphOrd n = ord n - ord 'A' + 1

triangle :: [Int]
triangle = scanl1 (+) [1..]

elemOrd :: Ord a => a -> [a] -> Bool
elemOrd _ [] = False
elemOrd n (x:xs)
  | n == x = True
  | n < x  = False
  | n > x  = elemOrd n xs

main :: IO ()
main = do
  f <- readFile "42.txt"
  let words = read ("[ " <> init f <> " ]") :: [String]
  let values = fmap wordValue words
  let exists = fmap (`elemOrd` triangle) values
  print $ length . filter (== True) $ exists

\end{code}


Champernowne's constant
=======================

Problem 40
----------

An irrational decimal fraction is created by concatenating the positive integers:

0.12345678910[1]112131415161718192021...

It can be seen that the 12th digit of the fractional part is 1.

If dn represents the nth digit of the fractional part, find the value of the following expression.

d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000

Solution
--------

\begin{code}

import Data.Char (digitToInt)

numberString :: String
numberString = concatMap show [0..]

expression :: [Int]
expression = map (\i -> digitToInt $ numberString !! i) $ map (10^) [0..]

main :: IO ()
main = print $ product $ take 7 expression

\end{code}


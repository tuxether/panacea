Goldbach's other conjecture
===========================

Problem 46
----------

It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + 2×1^2
15 = 7 + 2×2^2
21 = 3 + 2×3^2
25 = 7 + 2×3^2
27 = 19 + 2×2^2
33 = 31 + 2×1^2

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?

Solution
---------

\begin{code}

isPrime :: Integral a => a -> Bool
isPrime n = isPrime_ n primes
  where
    isPrime_ :: Integral a => a -> [a] -> Bool
    isPrime_ n (p:ps)
      | p * p > n      = True
      | n `mod` p == 0 = False
      | otherwise      = isPrime_ n ps

primes :: Integral a => [a]
primes = 2 : filter isPrime [3,5..]

oddComposites :: Integral a => [a]
oddComposites = filter (not . isPrime) [3,5..]

conjecture :: Bool
conjecture = length counterExamples == 0

isSquare :: Integral a => a -> Bool
isSquare = (\x -> fromIntegral (truncate x) == x) . sqrt . fromIntegral

validate :: Integral a => a -> Bool
validate n = validate_ n primes
  where
    validate_ :: Integral a => a -> [a] -> Bool
    validate_ n (p:ps)
      | p >= n  = False
      | otherwise = ( (n-p) `mod` 2 == 0 && isSquare ((n-p) `div` 2) ) || validate_ n ps

counterExamples :: Integral a => [a]
counterExamples = [ n | n <- oddComposites, not $ validate n ]

problem_46 = head counterExamples

main = print $ problem_46

\end{code}


Pandigital prime
================

Problem 41
----------

We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?

Solution
--------

\begin{code}

import Data.List (sort, permutations)
import Data.Char (intToDigit)

isPrime :: (Integral i) => i -> Bool
isPrime n = isPrime_ n primes
  where isPrime_ n (p:ps)
          | p * p > n      = True
          | n `mod` p == 0 = False
          | otherwise      = isPrime_ n ps

primes :: (Integral i) => [i]
primes = 2 : filter isPrime [3,5..]

main :: IO ()
main = print . maximum . filter isPrime $ pandigitalList
  where
    pandigitalList :: [Int]
    pandigitalList = [ read n | d <- [2..9], n <- permutations ['1'..intToDigit d] ]

\end{code}

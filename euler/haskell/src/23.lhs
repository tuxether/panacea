Non-abundant sums
=================

Problem 23
----------

A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.

\begin{code}

import qualified Data.Vector as V ((!), enumFromTo, Vector, map)

lastTerm :: Int
lastTerm = 28123

fastSumDivisors :: Int -> Int
fastSumDivisors n = (+1) $ sum $ [y | x <- [2..limit n]
                                    , mod n x == 0
                                    , let y = if x /= div n x then x + div n x else x]
  where limit = floor . sqrt . fromIntegral

isAbundantNumber :: Int -> Bool
isAbundantNumber n = fastSumDivisors n > n

boolMaps :: V.Vector Bool
boolMaps = V.map isAbundantNumber (V.enumFromTo 1 lastTerm)

abundantNumbers :: [Int]
abundantNumbers = filter isAbundantNumber [1..lastTerm]

rests :: Int -> [Int]
rests x = map (-1+x-) . takeWhile (<= x `div` 2) $ abundantNumbers

isSum :: Int -> Bool
isSum = any (boolMaps V.!) . rests

main :: IO ()
main = print . sum . filter (not . isSum) $ [1..lastTerm]

\end{code}


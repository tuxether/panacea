Distinct primes factors
=======================

Problem 47
----------

The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors each. What is the first of these numbers?

Solution
---------

\begin{code}

import Data.List (nub)

isPrime :: Integral a => a -> Bool
isPrime n = isPrime_ n primes
  where
    isPrime_ :: Integral a => a -> [a] -> Bool
    isPrime_ n (p:ps)
      | p * p > n      = True
      | n `mod` p == 0 = False
      | otherwise      = isPrime_ n ps

primes :: Integral a => [a]
primes = 2 : filter isPrime [3,5..]

primeFactors :: (Integral i) => i -> [i]
primeFactors n = factors n primes
  where
    factors n (x:xs)
      | x * x > n      = [n]
      | n `mod` x == 0 = x : factors (n `div` x) (x:xs)
      | otherwise      = factors n xs

problem_47 = head [ n | n <- [1..]
                      , all (==True)
                      . fmap ((==c) . length . nub . primeFactors)
                      $ [n..n+c-1] ]
  where
    c = 4

main = print $ problem_47

\end{code}


Lexicographic permutations
==========================

Problem 24
----------

A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?

Solution
--------

\begin{code}

import Data.List (permutations, sort)

digits :: [Int]
digits = [0..9]

lexicographicPermutations :: [Int] -> [[Int]]
lexicographicPermutations = sort . permutations

main :: IO ()
main = putStrLn $ concat $ map show $ (lexicographicPermutations digits) !! (1000000-1)

\end{code}

Digit fifth powers
==================

Problem 30
----------

Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

    1634 = 14 + 64 + 34 + 44
    8208 = 84 + 24 + 04 + 84
    9474 = 94 + 44 + 74 + 44

As 1 = 14 is not a sum it is not included.

The sum of these numbers is 1634 + 8208 + 9474 = 19316.

Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.

\begin{code}

import Data.Char (digitToInt)
import Control.Arrow ((&&&))

limit :: Int
limit = snd $ head $ dropWhile (\(a,b) -> a > b) $ map ((9^5*) &&& (10^)) $ [1..]

digitSumPower :: Int -> Int -> Int
digitSumPower p = sum . map (^p) . map digitToInt . show

main :: IO ()
main = print $ sum $ [i | i <- [2..limit], digitSumPower 5 i == i]

\end{code}


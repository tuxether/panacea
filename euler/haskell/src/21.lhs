Amicable numbers
================

Problem 21
----------

Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.

Solution
--------

\begin{code}

import Data.List (nub)

properDivisors :: Int -> [Int]
properDivisors n = [x | x <- 1:fastNonTrivialDivisors n, mod n x == 0]

fastNonTrivialDivisors :: Int -> [Int]
fastNonTrivialDivisors n = nub . concat $ [[x,div n x] | x <- [2..limit n], mod n x == 0]
  where limit = floor . sqrt . fromIntegral

sumProperDivisors :: Int -> Int
sumProperDivisors = sum . properDivisors

amicablePair :: Int -> Int -> Bool
amicablePair x y = f x == y && f y == x
  where f = sumProperDivisors

amicablePairs :: Int -> [Int]
amicablePairs n = nub . concat $ [ [x, m] | x <- [1..n]
                                          , let m = sumProperDivisors x
                                          , m > x
                                          , m <= 10000
                                          , amicablePair x m
                                 ]

main :: IO ()
main = print $ sum $ amicablePairs 10000

\end{code}


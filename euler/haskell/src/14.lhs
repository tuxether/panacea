Longest Collatz sequence
========================

Problem 14
----------

The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.

Solution
--------

\begin{code}

{-# OPTIONS_GHC -O2 -rtsopts -with-rtsopts=-K32m #-}

import Data.Word

collatz :: Int -> [Int]
collatz n
  | n == 1 = [n]
  | even n = n : collatz (n `div` 2)
  | odd n  = n : collatz (3 * n + 1)
  | otherwise = error "Collatz Problem has been disproved"

collatzLength :: Int -> Int
collatzLength n = collatzLength_ 0 n
  where collatzLength_ c 1 = c
        collatzLength_ c n = collatzLength_ (c+1) $ if even n then n `div` 2 else 3 * n + 1

greater a b
  | a > b     = a
  | otherwise = b

maxCollatzLengthPosition :: Int -> Int -> Int
maxCollatzLengthPosition start end = maxCollatzLengthPosition_ start end 0 0
  where maxCollatzLengthPosition_ c e m p
          | c == e              = if collatzLength c > m then c else p
          | m < collatzLength c = maxCollatzLengthPosition_ (c+1) e (collatzLength c) c
          | otherwise           = maxCollatzLengthPosition_ (c+1) e m p

main = print $ head $ collatz $ (maxCollatzLengthPosition 1 1000000)

\end{code}

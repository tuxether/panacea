Summation of primes
===================

Problem 10
-----------

The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

Solution
--------

\begin{code}

isPrime n = isPrime_ n primes
  where isPrime_ n (p:ps)
          | p * p > n      = True
          | n `mod` p == 0 = False
          | otherwise      = isPrime_ n ps

primes = 2 : filter isPrime [3,5..]

main = print $ sum $ takeWhile (< 2000000) primes

\end{code}

Reciprocal cycles
=================

Problem 26
----------

A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

    1/2  = 0.5
    1/3  = 0.(3)
    1/4  = 0.25
    1/5  = 0.2
    1/6  = 0.1(6)
    1/7  = 0.(142857)
    1/8  = 0.125
    1/9  = 0.(1)
    1/10 = 0.1

Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.

Solution
--------

The number of digits in the repeating portion of the decimal expansion of a rational number can be found directly from the multiplicative order of its denominator. 

When a rational number m/n with (m,n)=1 is expanded, the period begins after s terms and has length t, where s and t are the smallest numbers satisfying
   10^s=10^(s+t) (mod n).

When n ≢ 0 (mod 2, 5), s=0, and this becomes a purely periodic decimal with
   10^t=1 (mod n).

\begin{code}

g :: Integer -> Int
g n
  | n `mod` 2 == 0 = 0
  | n `mod` 5 == 0 = 0
  | otherwise = f n 1
  where f n i
          | mod (10^i) n == 1 = i
          | i > 1000 = 0
          | otherwise = f n (i+1)

main :: IO ()
main = print $ snd $ maximum [(g n, n) | n <- [3,5..1000-1]]

\end{code}


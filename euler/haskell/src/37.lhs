Truncatable primes
==================

Problem 37
-----------

The number 3797 has an interesting property. Being prime itself,
it is possible to continuously remove digits from left to right,
and remain prime at each stage: 3797, 797, 97, and 7.
Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right
and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.<F37>

Solution
---------

\begin{code}

isTruncablePrime :: Integer -> Bool
isTruncablePrime n = (_isTruncableLR . show) n
                   && _isTruncableRL n
                   && length (show n) /= 1
  where _isTruncableLR :: String -> Bool
        _isTruncableLR (x:[]) = isPrime . read $ (x:[])
        _isTruncableLR (x:xs) = (isPrime . read) (x:xs) && _isTruncableLR xs

        _isTruncableRL :: Integer -> Bool
        _isTruncableRL n
          | n `div` 10 == 0 = isPrime n
          | otherwise = isPrime n && _isTruncableRL (n `quot` 10)


isPrime :: Integer -> Bool
isPrime = _isPrime primes
  where _isPrime :: [Integer] -> Integer -> Bool
        _isPrime (p:ps) n
          | n < 2          = False
          | p * p > n      = True
          | n `mod` p == 0 = False
          | otherwise      = _isPrime ps n

primes :: [Integer]
primes = 2 : filter isPrime [3,5..]

main :: IO ()
main = print $ (sum . take 11) [ x | x <- [1..], isTruncablePrime x ]

\end{code}

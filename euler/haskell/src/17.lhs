Number letter counts
====================

Problem 17
----------

If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.

Solution
--------

\begin{code}

f "0"              = ""
f ('0':x)          = f x
f "1"              = "one"
f "2"              = "two"
f "3"              = "three"
f "4"              = "four"
f "5"              = "five"
f "6"              = "six"
f "7"              = "seven"
f "8"              = "eight"
f "9"              = "nine"
f "10"             = "ten"
f "11"             = "eleven"
f "12"             = "twelve"
f "13"             = "thirteen"
f "14"             = "fourteen"
f "15"             = "fifteen"
f "16"             = "sixteen"
f "17"             = "seventeen"
f "18"             = "eighteen"
f "19"             = "nineteen"
f ('2': x :[]    ) = "twenty"  ++ f (x:[])
f ('3': x :[]    ) = "thirty"  ++ f (x:[])
f ('4': x :[]    ) = "forty"   ++ f (x:[])
f ('5': x :[]    ) = "fifty"   ++ f (x:[])
f ('6': x :[]    ) = "sixty"   ++ f (x:[])
f ('7': x :[]    ) = "seventy" ++ f (x:[])
f ('8': x :[]    ) = "eighty"  ++ f (x:[])
f ('9': x :[]    ) = "ninety"  ++ f (x:[])
f ( x : "00"     ) = f (x:[])  ++ "hundred"
f ( x : y : z :[]) = f (x:[])  ++ "hundredand" ++ f (y:z:[])
f "1000"         = "onethousand"
f x = "undefined"

main = print $ sum . map (length . f . show) $ [1..1000]

\end{code}

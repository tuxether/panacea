Digit cancelling fractions
==========================

Problem 33
----------

The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.

Solution
--------

\begin{code}

import Data.Monoid ((<>))
import Data.Char (digitToInt)
import Data.Ratio ((%))

isCurious a b = a % b `elem` curiousDivisions
  where (a1:a2:[]) = map (digitToInt) . take 2 . show $ a
        (b1:b2:[]) = map (digitToInt) . take 2 . show $ b
        divisionA = if a1 == b2 && b1 /= 0 then [a2 % b1] else []
        divisionB = if a2 == b1 && b2 /= 0 then [a1 % b2] else []
        curiousDivisions = divisionA <> divisionB


curious = [ (a % b) | a <- [10..99], b <- [a+1..99], isCurious a b ]

main = print . product $ curious

\end{code}


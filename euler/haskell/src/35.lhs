Circular primes
===============

Problem 35
----------

The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?

Solution
--------

\begin{code}

import Data.List (tails, inits)
import Data.List.Extra (nubOrd)

primes = 2 : filter isPrime [3,5..]

isPrime :: Integer -> Bool
isPrime = _isPrime primes

_isPrime :: [Integer] -> Integer -> Bool
_isPrime (p:ps) n
  | p * p > n      = True
  | n `mod` p == 0 = False
  | otherwise      = _isPrime ps n

circlePermute n = nubOrd $ zipWith (++) (tails n) (inits n)

isCircularPrime = all (== True) . map (isPrime . read) . circlePermute . show

circularPrimes = [ p | p <- takeWhile (< 1000000) primes, isCircularPrime p ]

main = print $ length circularPrimes

\end{code}


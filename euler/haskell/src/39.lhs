Integer right triangles
=======================

Problem 39
-----------

If p is the perimeter of a right angle triangle with integral length sides,
{a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p ≤ 1000, is the number of solutions maximised?

Solution
--------

\begin{code}

import Data.List     (nub)
import Data.Foldable (maximumBy)
import Data.Function (on)

factorPairs :: Int -> [(Int, Int)]
factorPairs n = [ (a,b)
                | a <- [1 .. (floor . sqrt . fromIntegral) n]
                , let b = n `div` a
                , a * b == n
                , a <= b
                ]

-- | The Dickson’s method for generating Pythagorean triples states that the
-- integers a = r + s, b = r + t, c = r + s + t; form a Pythagorean triple
-- (a, b,c) on condition that r^2 = 2*s*t, where r,s,t are positive integers. 
dicksonTriples :: Int -> [(Int, Int, Int)]
dicksonTriples n = [ (r+s, r+t, r+s+t)
                   | r <- [1..n `div` 2]
                   , even (r^2)
                   , (s, t) <- factorPairs (r^2 `div` 2)
                   , r^2 == 2*s*t
                   , let a = r+s
                   , let b = r+t
                   , let c = r+s+t
                   , a <= b
                   , a + b + c == n
                   ]

main = print $ (\(a,b,c) -> a+b+c)
             . head
             . maximumBy (compare `on` length)
             $ map dicksonTriples [1..1000]

\end{code}


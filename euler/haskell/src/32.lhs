Pandigital products
===================

Problem 32
----------

We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.

HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.

Solution
--------

The answer needs to be either a 1-digit number times a 4 digit number, or a 2-digit number times a 3-digit number. This fact is used to restrict the search space.

\begin{code}

import Data.List       (sort, intersect)
import Data.List.Extra (nubOrd)
import Data.Word       (Word)

isPandigital :: Word -> Word -> Word -> Bool
isPandigital x y z = sort (show x ++ show y ++ show z) == "123456789"

oneDigitTimesFourDigits :: [Word]
oneDigitTimesFourDigits   = [z | x <- [1..9],   y <- [1234..9876], let z = x * y, isPandigital x y z]

twoDigitsTimesThreeDigits :: [Word]
twoDigitsTimesThreeDigits = [z | x <- [12..98], y <- [123..987],   let z = x * y, isPandigital x y z]

main :: IO ()
main = print $ sum . nubOrd $ oneDigitTimesFourDigits ++ twoDigitsTimesThreeDigits

\end{code}


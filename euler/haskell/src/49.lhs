Prime permutations
==================

Problem 49
----------

The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?

Solution
---------

\begin{code}

import Data.List (sort, nub)
import qualified Data.Map as M (delete, filter, toList, fromListWith, Map)

isPrime :: Integral a => a -> Bool
isPrime n = isPrime_ n primes
  where
    isPrime_ :: Integral a => a -> [a] -> Bool
    isPrime_ n (p:ps)
      | p * p > n      = True
      | n `mod` p == 0 = False
      | otherwise      = isPrime_ n ps

primes :: Integral a => [a]
primes = 2 : filter isPrime [3,5..]

fourDigitPrimes :: Integral a => [a]
fourDigitPrimes = takeWhile (<10000) . dropWhile (< 1000) $ primes

hashes :: [String]
hashes = fmap (sort . show) fourDigitPrimes

hashMap :: Integral a => M.Map String [a]
hashMap = M.fromListWith (++) $ zipWith (\a b -> (a, [b])) hashes fourDigitPrimes

progression :: Integral a => [a] -> [(a, a, a)]
progression xs
  | length xs < 3 = []
  | otherwise = [ (a, b, c) | a <- xs, b <- xs, c <- xs
                            , a < b && b < c
                            , b - a == c - b ]

problem_49 = (\(a,b,c) -> show a ++ show b ++ show c)
           . head
           . snd
           . head
           . M.toList
           . M.delete "1478"
           . M.filter (not . null)
           . fmap progression
           $ hashMap

main = print $ problem_49

\end{code}


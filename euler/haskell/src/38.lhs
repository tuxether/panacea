Pandigital multiples
====================

Problem 38
-----------

Take the number 192 and multiply it by each of 1, 2, and 3:

192 × 1 = 192
192 × 2 = 384
192 × 3 = 576
By concatenating each product we get the 1 to 9 pandigital, 192384576.
We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5,
giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as
the concatenated product of an integer with (1,2, ... , n) where n > 1?

Solution
---------

\begin{code}

import Data.List (sort)

isPandigital :: Integer -> Bool
isPandigital n = (sort . show) n == "123456789"

-- | The condition n <= 9999 must be satisfied since 9999 produces the last
-- 9 digit number with [1..] where the length of [1..] is at least 2.
pandigitalMultiple :: Integer -> Integer
pandigitalMultiple n = read . take 9 . concat $ [ show x | i <- [1..], let x = n * i ]

isPandigitalMultiple :: Integer -> Bool
isPandigitalMultiple n = isPandigital . pandigitalMultiple $ n

main :: IO ()
main = print $ maximum [ pandigitalMultiple i | i <- [1..9999], isPandigitalMultiple i]

\end{code}


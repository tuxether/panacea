Counting Sundays
================

Problem 19
----------

You are given the following information, but you may prefer to do some research for yourself.

* 1 Jan 1900 was a Monday.
* Thirty days has September,
  April, June and November.
  All the rest have thirty-one,
  Saving February alone,
  Which has twenty-eight, rain or shine.
  And on leap years, twenty-nine.
* A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

Solution
--------

\begin{code}

{-# LANGUAGE BangPatterns #-}

nonLeapYear :: [Int]
nonLeapYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

leapYear :: [Int]
leapYear = 31 : 29 : drop 2 nonLeapYear

getYear :: Int -> [Int]
getYear year | year `mod` 400 == 0 = leapYear
             | year `mod` 100 == 0 = nonLeapYear
             | year `mod` 4   == 0 = leapYear
             | otherwise           = nonLeapYear

countSundays :: Int -> [Int] -> Int
countSundays currentDay = length . filter (==0) . map (`mod` 7) . map (+currentDay) . scanl1 (+)

f :: Int -> Int -> Int -> Int
f !currentYear !dayCount !sundayCount
  | currentYear < 1901 = f (currentYear + 1) (dayCount + sum (getYear currentYear)) sundayCount
  | currentYear > 2000 = sundayCount
  | otherwise = f (currentYear + 1) (dayCount + sum (getYear currentYear)) (sundayCount + countSundays dayCount (getYear currentYear))

main :: IO ()
main = print $ f 1900 1 0

\end{code}

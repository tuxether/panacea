Coin sums
=========

Problem 31
----------

In England the currency is made up of pound, £, and pence, p,
and there are eight coins in general circulation:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
It is possible to make £2 in the following way:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
How many different ways can £2 be made using any number of coins?

Solution
--------

\begin{code}

main :: IO ()
main = print $ ways coins 200

coins :: [Int]
coins = reverse [1, 2, 5, 10, 20, 50, 100, 200]

-- | Traverse all possible paths. If the remainder in the end is zero,
--   then count that possibility, else, ignore.
ways :: [Int] -> Int -> Int
ways [] 0 = 1
ways [] _ = 0
ways (c:cs) n = sum [ ways cs (n - i * c) -- use i coins of value c
                    | i <- [0 .. div n c] -- number of coins of value c
                    ]

\end{code}

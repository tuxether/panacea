-- |
-- Module: Main
-- Copyright: (C) 2018 Vi Jay Suskind
-- License: AGPLv3 (see the file COPYING)
-- 
-- Maintainer: tuxether@protonmail.ch
-- Stability: experimental
-- Portability: non-portable (GHC Extensions)
--
-- Shake file for building and running solutions
--

{-# LANGUAGE ScopedTypeVariables #-}

import Development.Shake
import Development.Shake.Command
import Development.Shake.FilePath
import Development.Shake.Util

import Control.Monad (forM_)
import Data.Monoid ((<>))

shakeDir :: FilePath
shakeDir = "src"

-- 22 needs split
-- 23 needs vector

completed :: [Int]
completed = [1..21] <> [24..50]

main :: IO ()
main = shakeArgs shakeOptions{shakeFiles=shakeDir} $ do
    want [ shakeDir </> show n <.> "exe" | n <- completed ]

    shakeDir </> "*.exe" %> \out -> do
        let src = out -<.> "lhs"
        need [src]
        cmd "ghc -O2" src "-o" out

    "clean" ~> do
        removeFilesAfter shakeDir ["*.o", "*.hi", "*.exe"]

    "time" ~> do
        () <- forM_ completed $ \n -> do
            putNormal $ "Result and execution time of problem " <> show n
            let src = shakeDir </> show n <.> "exe"
            () <- cmd "/usr/bin/time -f %e" src
            putNormal " "
            pure ()
        pure ()


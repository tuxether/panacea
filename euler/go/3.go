// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"math/big"
)

func generate(c chan<- big.Int) {
	step := big.NewInt(2)

	c <- *big.NewInt(2)
	for i := big.NewInt(3); ; i.Add(i, step) {
		tmp := big.NewInt(0)
		tmp.Set(i)
		c <- *tmp
	}
}

func filter(in <-chan big.Int, out chan<- big.Int, prime big.Int) {
	z := big.NewInt(0)
	tmp := big.NewInt(0)
	for {
		v := <-in

		if tmp.Mod(&v, &prime); tmp.Cmp(z) != 0 {
			out <- v
		}
	}
}

func main() {
	ch := make(chan big.Int)

	go generate(ch)

	factor := big.NewInt(0)
	z := big.NewInt(0)
	tmp := big.NewInt(0)

	number := big.NewInt(600851475143)

	for {
		prime := <-ch

		if tmp.Mul(&prime, &prime); tmp.Cmp(number) > 0 {
			fmt.Println(number.String())
			return
		}

		if tmp.Mod(number, &prime); tmp.Cmp(z) == 0 {
			factor.Set(&prime)
			tmp.Div(number, &prime)
			number.Set(tmp)
		}

		chF := make(chan big.Int)
		go filter(ch, chF, prime)
		ch = chF
	}
}

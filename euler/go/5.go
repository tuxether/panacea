// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
)

func lcm(t1, t2 int) int {
	lcm := 1

	if t1 > t2 {
		lcm = t1
	} else {
		lcm = t2
	}

	for {
		if lcm%t1 == 0 && lcm%t2 == 0 {
			return lcm
		}
		lcm++
	}
}

func main() {
	n := 1
	for i := 1; i <= 20; i++ {
		n = lcm(n, i)
	}
	fmt.Println(n)
}

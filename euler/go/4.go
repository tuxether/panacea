// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"container/list"
	"fmt"
	"strconv"
)

// Reverse returns its argument string reversed rune-wise left to right.
func reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

func isPalindrome(n int) bool {
	s := strconv.Itoa(n)
	return s == reverse(s)
}

func main() {
	low := 100
	high := 999
	l := list.New()
	for i := high; i >= low; i-- {
		for j := low; j <= i; j++ {
			if isPalindrome(i * j) {
				l.PushFront(i * j)
			}
		}
	}
	max := 0
	for e := l.Front(); e != nil; e = e.Next() {
		tmp := e.Value.(int)
		if max < tmp {
			max = tmp
		}
	}
	fmt.Println(max)
}

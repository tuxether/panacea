// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
)

const upperBound = 1000 - 1

func comp(n int) int {
	sum := func(n int) int {
		return n * (n + 1) / 2
	}

	threes := 3 * sum(n/3)
	fives := 5 * sum(n/5)
	fifteens := 15 * sum(n/15)

	return threes + fives - fifteens
}

func main() {
	fmt.Printf("%d\n", comp(upperBound))
}

// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"math"
)

func main() {
	var n uint64 = 100
	sqOfSum := uint64(math.Pow(float64(n*(n+1)/2), float64(2)))
	sumOfSq := n * (n + 1) * (2*n + 1) / 6
	sum := sqOfSum - sumOfSq
	fmt.Println(sum)
}

// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
)

const upperBound = 4e6

func fibonacci() func() int {
	a := 1
	b := 1
	var c int

	f := func() int {
		c = a + b
		a = b
		b = c
		return a
	}

	return f
}

func sum() int {
	s := 0
	f := fibonacci()

	var v int
	for ; v <= upperBound; v = f() {
		if v%2 == 0 {
			s += v
		}
	}

	return s
}

func main() {
	finished := sum()

	fmt.Printf("%d\n", finished)
}

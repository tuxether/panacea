// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"math"
)

func main() {
	for i := 1; i < 1000; i++ {
		for j := i; j < 1000; j++ {
			k := math.Hypot(float64(i), float64(j))
			if float64(int(k)) == k && i+j+int(k) == 1000 {
				abc := i * j * int(k)
				fmt.Println(abc)
			}
		}
	}
}

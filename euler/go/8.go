// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	const path = "../data/8.txt"
	f, err := os.Open(path)
	if err != nil {
		log.Fatal("Unable to open file: ", err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanLines)

	var buffer bytes.Buffer
	for scanner.Scan() {
		buffer.WriteString(scanner.Text())
	}

	max := int64(0)
	bytes := buffer.Bytes()
	for i := 0; i < buffer.Len()-13; i++ {
		product := int64(1)
		for j := 0; j < 13; j++ {
			n, err := strconv.ParseInt(string(bytes[i+j]), 10, 32)
			if err != nil {
				log.Fatal("Error: ", err)
			}
			product *= n
		}
		if product > max {
			max = product
		}
	}

	fmt.Printf("%v\n", max)
}

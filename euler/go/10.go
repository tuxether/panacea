// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
)

func sieveOfEratosthenes(n int) (primes []int) {
	b := make([]bool, n)
	for i := 2; i < n; i++ {
		if b[i] {
			continue
		}
		primes = append(primes, i)
		for k := i * i; k < n; k += i {
			b[k] = true
		}
	}
	return
}

func main() {
	sum := uint64(0)
	primes := sieveOfEratosthenes(2e6)
	for _, p := range primes {
		sum += uint64(p)
	}
	fmt.Println(sum)
}

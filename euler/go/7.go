// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
)

func generate() chan uint64 {
	c := make(chan uint64)
	go func() {
		for i := uint64(2); ; i++ {
			c <- i
		}
	}()
	return c
}

func filter(in <-chan uint64, out chan<- uint64, prime uint64) {
	for {
		if v := <-in; v%prime != 0 {
			out <- v
		}
	}
}

func sieveFilter() chan uint64 {
	out := make(chan uint64)
	go func() {
		gen := generate()
		for {
			prime := <-gen
			out <- prime
			f := make(chan uint64)
			go filter(gen, f, prime)
			gen = f
		}
	}()
	return out
}

func sieve() chan uint64 {
	out := make(chan uint64)

	go func() {
		g := generate()

		p := <-g
		out <- p

		basePrimes := sieveFilter()
		bp := <-basePrimes
		bSq := bp * bp

		for {
			if p := <-g; p == bSq {
				f := make(chan uint64)
				go filter(g, f, bp)
				g = f

				bp = <-basePrimes
				bSq = bp * bp
			} else {
				out <- p
			}
		}
	}()

	return out
}

func main() {
	ch := sieve()

	for i := 0; i < 10000; i++ {
		<-ch
	}
	prime := <-ch
	fmt.Println(prime)
}
